<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Messages Log</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-combobox.css" rel="stylesheet">
    <link href="css/datatables.min.css" rel="stylesheet">
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <script type="text/javascript" src="js/jquery-3.3.1.slim.min.js"></script>    
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-combobox.js"></script>
	<script type="text/javascript" >
	$(document).ready(function() 
	{
		$('#clear').click( function(e)
		{			
			e.preventDefault();
			var def_from = $('#def_from').val();
			var def_to = $('#def_to').val();
			$('#log_form').trigger("reset");
			
			$('#from_date').val(def_from);
			$('#to_date').val(def_to);	
			
			$('#country option[value=""]').attr("selected", true);
			$("input[name='country']").val('');
			
			$("#user option[value='']").attr("selected", true);
			$("input[name='user']").val('');
		});	
	});
	</script>
</head>
<body>

<div class="container">

<h3 class="text-primary" style="padding-top:30px;">Messages Log</h3>
		
	<div class="jumbotron">		
		<form method="POST" id="log_form" action="index.php" class="form-horizontal pull-left" > 
			
			<div class="form-group row">					
					<label for="from_date" class="col-sm-2 control-label text-primary">From Date:</label>				    				
					<input   id="from_date" name="from_date" type="date" value="<?php echo $from_date; ?>">
			</div>
			
			<div class="form-group row">					
					<label for="from_date" class="col-sm-2 control-label text-primary">To Date:</label>				    				
					<input   id="to_date" name="to_date" type="date" value="<?php echo $to_date; ?>">
			</div>
			
			<div class="form-group row">
				<label for="country"   class="col-sm-2 control-label text-primary" >Select Country</label>
				<select class="form-control col-sm-10" id="country" name="country">
				  <option value="">Select Country</option>
				  <?php 				  
				  foreach($countries as $country) :?>
				   <option value="<?php echo $country['cnt_id']; ?>" <?php if($country['cnt_id']==$country_id) echo "selected"; ?> ><?php echo $country['cnt_title']; ?></option>
				  <?php endforeach; ?>			
				</select>
			</div>
			
			<div class="form-group row">
				<label for="user"   class="col-sm-2 control-label text-primary" >Select User</label>
				<select class="form-control col-sm-10" id="user" name="user">
				  <option value="">Select User</option>
				  <?php 				  
				  foreach($users as $user) :?>
				   <option value="<?php echo $user['usr_id']; ?>" <?php if($user['usr_id']==$user_id) echo "selected"; ?> ><?php echo $user['usr_name']; ?></option>
				  <?php endforeach; ?>			
				</select>
			</div>
			
			<div class="form-group row">
				<div class="col-sm-1">
				<button type="submit" class="btn btn-primary">Submit</button>
				</div>
				<div class="col-sm-2">
				<button type="reset" class="btn btn-info" id="clear">Clear form</button>
				</div>
			</div>
			<div>
			    <input type="hidden" id="def_from" value="<?php echo $DEFAULT_FROM_DATE; ?>">
				<input type="hidden" id="def_to" value="<?php echo $DEFAULT_TO_DATE; ?>">
			</div>
		</form>
  </div>
<p></p>

<table id="msg_log" class="table table-striped table-bordered table-hover" >
	<thead><tr class="table-primary"><th>Date</th><th>Successfully Sent</th><th>Failed</th></tr></thead>
	<tbody>
	<?php
	foreach($rows as $row): ?>
	<tr><td><?php echo $row['sent_on']; ?></td><td><?php echo $row['total_sent']; ?></td><td><?php echo $row['total_failed']; ?></td></tr>
	<?php endforeach; ?>
	</tbody>
</table>
</div>
 
 <script src="js/bootstrap.min.js"></script>
 <script src="js/datatables.min.js"></script>
 <script type="text/javascript" src="js/jquery_plugins.js"></script>
</body>
</html>
<?php
include_once "config/config.php";
include_once "models/database.php";
include_once "models/base_model.php";
include_once "models/msg_model.php";
include_once "models/country_model.php";
include_once "models/user_model.php";
include_once "controllers/msg_controller.php";

try
{	
	$controller = new Msg_Controller();
	$controller->show_msg_log();  
}
catch(Exception $e)
{
	echo $e->getMessage();
}
?>
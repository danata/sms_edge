<?php
class Msg_Controller
{
	private $msg_model;
	private $country_model;
	private $user_model;
	
	
   public function __construct()
   {
	   $this->msg_model = new Msg_Model();
	   $this->country_model = new Country_Model();
	   $this->user_model = new User_Model();	   	
   }
   
   
   public function show_msg_log()
   {
	   global $DEFAULT_FROM_DATE,$DEFAULT_TO_DATE ;
	   
	    $from_date = empty($_POST['from_date']) ? "2018-10-01": $_POST['from_date'];
		$to_date = empty($_POST['to_date']) ? "2018-11-01":$_POST['to_date'];
		$country_id = empty($_POST['country']) ? '': $_POST['country'];
		$user_id = empty($_POST['user'] ) ? '':$_POST['user'];
				
		$rows = $this->msg_model->get_aggregated_info($from_date,$to_date,$country_id,$user_id);
		$countries = $this->country_model->get_all_countries();
		$users = $this->user_model->get_all_users();
		include_once 'views/display_log.php';
   }
   
}

?>
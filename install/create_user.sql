

CREATE USER 'msg_user'@'localhost' IDENTIFIED BY 'argtk522bmg';
GRANT SELECT ON messages.users TO 'msg_user'@'localhost';
GRANT SELECT ON messages.countries TO 'msg_user'@'localhost';
GRANT SELECT ON messages.numbers TO 'msg_user'@'localhost';
GRANT SELECT ON messages.send_log TO 'msg_user'@'localhost';
GRANT SELECT ON messages.send_log_aggregated TO 'msg_user'@'localhost';

FLUSH PRIVILEGES;
--
-- Database: `messages`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `cnt_id` int(11) NOT NULL AUTO_INCREMENT,
  `cnt_code` varchar(10) NOT NULL,
  `cnt_title` varchar(50) NOT NULL,
  `cnt_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cnt_id`),
  UNIQUE KEY `cnt_title_idx` (`cnt_title`),
  UNIQUE KEY `cnt_code_idx` (`cnt_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`cnt_id`, `cnt_code`, `cnt_title`, `cnt_created`) VALUES
(1, '355', 'Albania', '2018-10-12 15:50:48'),
(2, '32', 'Belgium', '2018-10-12 15:51:34'),
(3, '86', 'China', '2018-10-12 15:52:02'),
(4, '358', 'Finland', '2018-10-12 15:52:44'),
(5, '30', 'Greece', '2018-10-12 15:53:17'),
(6, '852', 'Hong Kong', '2018-10-14 07:09:58'),
(7, '36', 'Hungary', '2018-10-14 07:10:51'),
(8, '972', 'Israel', '2018-10-14 07:11:17'),
(9, '39', 'Italy', '2018-10-14 07:11:36'),
(10, '81', 'Japan', '2018-10-14 07:12:00'),
(11, '254', 'Kenya', '2018-10-14 07:12:22'),
(12, '961', 'Lebanon', '2018-10-14 07:13:42'),
(13, '389', 'Macedonia', '2018-10-14 07:14:22'),
(14, '356', 'Malta', '2018-10-14 07:17:37'),
(15, '977', 'Nepal', '2018-10-14 07:17:56'),
(16, '51', 'Peru', '2018-10-14 07:18:13'),
(17, '63', 'Philippines', '2018-10-14 07:18:34');

-- --------------------------------------------------------

--
-- Table structure for table `numbers`
--

DROP TABLE IF EXISTS `numbers`;
CREATE TABLE IF NOT EXISTS `numbers` (
  `num_id` int(11) NOT NULL AUTO_INCREMENT,
  `cnt_id` int(11) NOT NULL,
  `num_number` varchar(20) NOT NULL,
  `num_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`num_id`),
  KEY `cnt_idx` (`cnt_id`),
  KEY `num_number_idx` (`num_number`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `numbers`
--

INSERT INTO `numbers` (`num_id`, `cnt_id`, `num_number`, `num_created`) VALUES
(1, 5, '334456789', '2018-10-12 15:56:27'),
(2, 2, '0575676345543', '2018-10-12 15:57:52'),
(3, 3, '34435667567', '2018-10-12 15:58:27'),
(4, 5, '45353478678', '2018-10-12 15:58:47');

-- --------------------------------------------------------

--
-- Table structure for table `send_log`
--

DROP TABLE IF EXISTS `send_log`;
CREATE TABLE IF NOT EXISTS `send_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `cnt_id` int(11) NOT NULL,
  `log_message` text NOT NULL,
  `log_success` tinyint(1) NOT NULL,
  `log_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`),
  KEY `usr_id_idx` (`usr_id`),
  KEY `cnd_id_idx` (`cnt_id`),
  KEY `log_success_idx` (`log_success`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `send_log`
--

INSERT INTO `send_log` (`log_id`, `usr_id`, `cnt_id`, `log_message`, `log_success`, `log_created`) VALUES
(3, 1, 2, 'hi from pnina', 1, '2018-10-13 07:34:06');

-- --------------------------------------------------------

--
-- Table structure for table `send_log_aggregated`
--

DROP TABLE IF EXISTS `send_log_aggregated`;
CREATE TABLE IF NOT EXISTS `send_log_aggregated` (
  `usr_id` int(11) NOT NULL,
  `cnt_id` int(11) NOT NULL,
  `sent_on` date NOT NULL,
  `sent_ok` int(11) NOT NULL,
  `failed` int(11) NOT NULL,
  PRIMARY KEY (`usr_id`,`cnt_id`,`sent_on`),
  KEY `usr_id_idx` (`usr_id`),
  KEY `cnt_id_idx` (`cnt_id`),
  KEY `sent_on_idx` (`sent_on`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `send_log_aggregated`
--

INSERT INTO `send_log_aggregated` (`usr_id`, `cnt_id`, `sent_on`, `sent_ok`, `failed`) VALUES
(1, 2, '2018-10-13', 8, 2),
(1, 3, '2018-10-02', 60, 21),
(1, 5, '2018-10-12', 6, 3),
(1, 8, '2018-10-01', 10, 7),
(1, 13, '2018-10-09', 200, 100),
(2, 1, '2018-10-10', 12, 5),
(2, 3, '2018-10-02', 10, 5),
(2, 4, '2018-10-10', 7, 3),
(2, 10, '2018-10-04', 34, 11),
(2, 13, '2018-10-08', 56, 23),
(3, 1, '2018-10-13', 79, 23),
(3, 3, '2018-10-03', 4, 2),
(3, 5, '2018-10-03', 4, 6),
(3, 9, '2018-10-01', 15, 4),
(3, 11, '2018-10-06', 98, 34),
(3, 14, '2018-10-11', 78, 22),
(4, 2, '2018-10-12', 45, 21),
(4, 11, '2018-10-05', 67, 22),
(4, 12, '2018-10-07', 45, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `usr_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_name` varchar(30) COLLATE utf8_bin NOT NULL,
  `usr_active` tinyint(1) NOT NULL,
  `usr_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`usr_id`),
  UNIQUE KEY `usr_name_idx` (`usr_name`),
  KEY `usr_active` (`usr_active`),
  KEY `usr_created_idx` (`usr_created`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`usr_id`, `usr_name`, `usr_active`, `usr_created`) VALUES
(1, 'pnina@gmail.com', 1, '2018-10-12 15:53:56'),
(2, 'nir@gmail.com', 0, '2018-10-12 15:54:17'),
(3, 'tomer@gmail.com', 1, '2018-10-12 15:54:49'),
(4, 'lior@gmail.com', 1, '2018-10-12 15:55:23');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `numbers`
--
ALTER TABLE `numbers`
  ADD CONSTRAINT `numbers_ibfk_1` FOREIGN KEY (`cnt_id`) REFERENCES `countries` (`cnt_id`);

--
-- Constraints for table `send_log`
--
ALTER TABLE `send_log`
  ADD CONSTRAINT `send_log_ibfk_1` FOREIGN KEY (`cnt_id`) REFERENCES `countries` (`cnt_id`),
  ADD CONSTRAINT `send_log_ibfk_2` FOREIGN KEY (`usr_id`) REFERENCES `users` (`usr_id`);

--
-- Constraints for table `send_log_aggregated`
--
ALTER TABLE `send_log_aggregated`
  ADD CONSTRAINT `send_log_aggregated_ibfk_1` FOREIGN KEY (`usr_id`) REFERENCES `users` (`usr_id`),
  ADD CONSTRAINT `send_log_aggregated_ibfk_2` FOREIGN KEY (`cnt_id`) REFERENCES `countries` (`cnt_id`);
COMMIT;
